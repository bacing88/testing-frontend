import { useQuery } from "react-query";
import axios from "axios";

function getMostPopular() {
  const getMost = async () => {
    return await axios.get(
      "https://api.nytimes.com/svc/mostpopular/v2/emailed/1.json?api-key=GYTWZG1F4BV2ZQ4GVlKPGt7nlcp6YCW0"
    );
  };
  return useQuery("Most", () => getMost(), {
    enabled: true,
  });
}

export default getMostPopular;
