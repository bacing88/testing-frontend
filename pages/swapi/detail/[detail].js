import React from "react";
import { useQuery } from "react-query";
import { useRouter } from "next/router";
import CardDetailSwapi from "../../../components/content/cardDetailSwapi";
import axios from "axios";

function detail() {
  const router = useRouter();
  const { detail } = router.query;
  const getIdSwapi = async ({ pageParam = detail }) => {
    return axios.get(`https://swapi.dev/api/planets/${pageParam}`);
  };
  const { isLoading, isError, data, error } = useQuery(
    ["swapi", { detail }],
    getIdSwapi
  );
  if (isLoading) {
    return (
      <h2 className="flex justify-center mt-12 font-bold text-3xl text-gray-900">
        Loading...
      </h2>
    );
  }
  if (isError) {
    return (
      <h2 className="flex justify-center mt-12  text-3xl text-red-400">
        {error.message}
      </h2>
    );
  }
  return (
    <>
      <div className="flex flex-col justify-center items-center p-12">
        <button
          onClick={() => router.push("/swapi")}
          className="p-2 rounded-sm w-56 bg-gray-800 text-white txt-white mb-2"
        >
          Back
        </button>
        <CardDetailSwapi data={data?.data} />
      </div>
    </>
  );
}

export default detail;
