import React, { useState } from "react";
import Layout from "../../components/LayoutSwapi";
import CardSwapi from "../../components/content/cardSwapi";
import { useInfiniteQuery, QueryCache } from "react-query";
import axios from "axios";

const getSwapi = async ({ pageParam = 1 }) => {
  return axios.get(`https://swapi.dev/api/planets?page=${pageParam}`);
};
const queryCache = new QueryCache({
  onError: (error) => {
    console.log(error);
  },
  onSuccess: (data) => {
    console.log("datacache", data);
  },
});

const query = queryCache.clear();
console.log(typeof query);
function index() {
  const [pageTotal, setPageTotal] = useState(0);
  const {
    isLoading,
    isError,
    error,
    data,
    fetchNextPage,
    isFetching,
    isFetchingNextPage,
  } = useInfiniteQuery(["swapi"], getSwapi, {
    getNextPageParam: (lastPage, pages) => {
      const dataPage = lastPage.data.next;
      const dataId = dataPage?.split("=");
      return Number(dataId[1]);
    },
  });
  if (isLoading) {
    return (
      <h2 className="flex justify-center mt-12 font-bold text-3xl text-gray-900">
        Loading...
      </h2>
    );
  }
  if (isError) {
    return (
      <h2 className="flex justify-center mt-12  text-3xl text-red-400">
        {error.message}
      </h2>
    );
  }
  const totalData = data.pages[0].data.count;
  console.log(data.pages.length == totalData / 10 - 1);

  return (
    <Layout>
      <div className="grid grid-col-1 md:grid-cols-2 lg:grid-cols-2 lg:gap-1 p-2">
        {data.pages.map((page) =>
          page.data.results.map((item, index) => (
            <div key={index}>
              <CardSwapi data={item} />
            </div>
          ))
        )}
      </div>
      <div className="flex justify-center p-4">
        <button
          className="px-12 py-1 bg-gray-800 text-white rounded-sm"
          onClick={fetchNextPage}
          disabled={data.pages.length == totalData / 10 - 1}
        >
          {data.pages.length == totalData / 10 - 1
            ? `Data Maximal : ${totalData - 10}`
            : "Load More"}
        </button>
      </div>
    </Layout>
  );
}

export default index;
