import Image from "next/image";
import Link from "next/link";
import React, { useState, useEffect } from "react";
export default function Home() {
  const [data, setData] = useState(null);
  const [listData, setListData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [err, setErr] = useState("");

  const usersData = [];
  const [listUser, setListUser] = useState(usersData);
  console.log(listUser);
  const initialFormState = { id: null, name: "", username: "" };
  const [user, setUser] = useState(initialFormState);

  useEffect(() => {
    fetch("https://api.kanye.rest/")
      .then((response) => response.json())
      .then((data) => setData(data.quote));
  }, []);
  const handleClick = async () => {
    setIsLoading(true);
    try {
      const response = await fetch("https://api.kanye.rest/", {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      });
      if (!response.ok) {
        throw new Error(`Error! status: ${response.status}`);
      }
      const result = await response.json();
      setData(result.quote);
    } catch (err) {
      setErr(err.message);
    } finally {
      setIsLoading(false);
    }
  };
  const handleAddVaforit = () => {
    setListData((oldArray) => [...oldArray, data]);
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setUser({ ...user, [name]: value });
  };
  const addUser = (user) => {
    user.id = user.length + 1;
    setListUser([...listUser, user]);
  };

  return (
    <>
      <section className="text-gray-600">
        <div className="max-w-screen-xl px-4 py-16 mx-auto sm:px-6 lg:px-8 space-y-2">
          <Link href={"https://api.kanye.rest/"}>
            <a
              target="_blank"
              className="absolute top-6 left-8 underline text-md text-blue-500 uppercase"
            >
              Api Rest
            </a>
          </Link>
          <div className="max-w-lg mx-auto text-center">
            <Image
              src="https://images.businessoffashion.com/profiles/asset/1797/43897e2e4a6d155d72dd9df352017b546ef9e229.jpeg"
              alt="image"
              className=" object-cover"
              width={200}
              height={200}
            />
            <h2 className="text-3xl font-bold">Ibrahim</h2>
          </div>
          <div className="max-w-lg mx-auto text-center">
            <h2 className="text-lg font-bold">{data}</h2>
          </div>
          <div className="flex justify-center space-x-2 max-w-lg mx-auto text-center">
            <button
              onClick={handleClick}
              className="px-2 py-0.5 text-white bg-green-500 rounded-sm"
            >
              Get Quote
            </button>
            <button
              onClick={handleAddVaforit}
              className="px-2 py-0.5 text-white bg-green-500 rounded-sm"
              disabled={listData.includes(data)}
            >
              Add Favorite
            </button>
          </div>
          <div className="flex space-x-2 max-w-xl mx-auto text-left p-2 ">
            <ol className="list-disc">
              {listData.map((item, index) => (
                <li key={index}>{item}</li>
              ))}
            </ol>
          </div>

          <div className="flex flex-col space-x-2 max-w-xl mx-auto text-center border border-collapse border-gray-700">
            <div className="p-2">
              <form
                onSubmit={(event) => {
                  event.preventDefault();
                  if (!user.name) return;
                  addUser(user);
                  setUser(initialFormState);
                }}
              >
                <div className="flex p-4">
                  <input
                    className="w-full px-2 py-1 text-sm border border-gray-400 outline-none"
                    placeholder="Add User..."
                    type="text"
                    name="name"
                    value={user.name}
                    onChange={handleInputChange}
                  />
                  <button className="w-32 px-2 py-0.5 text-white bg-green-500">
                    Submit
                  </button>
                </div>
              </form>
              <ol className="list-disc list-inside text-left px-6">
                {listUser.length > 0 ? (
                  listUser.map((user) => (
                    <div key={user.id}>
                      <li>{user.name}</li>
                    </div>
                  ))
                ) : (
                  <p className="text-red-400 text-md">No users ..!</p>
                )}
              </ol>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
