import React, { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import CardArticleBusiness from "../../components/content/cardArticle";
import { useQuery, QueryCache } from "react-query";
import axios from "axios";
import Artikel from "../../data.json";
function index() {
  //const [loadData, setLoadData] = useState(6);
  const [dataId, setDataId] = useState(null);
  useEffect(() => {
    setDataId("testing");
  }, []);
  const getMost = async () => {
    return axios.get(
      "https://api.themoviedb.org/3/discover/movie?api_key=1be977bc7a3b44902b24b392280dca23&primary_release_year=2017&sort_by=revenue.desc"
    );
  };

  const { isLoading, isError, data, error, status } = useQuery(
    "mosts",
    getMost
  );

  if (isLoading) {
    return (
      <h2 className="flex justify-center mt-12 font-bold text-3xl text-gray-900">
        Loading...
      </h2>
    );
  }

  if (isError) {
    return (
      <h2 className="flex justify-center mt-12  text-3xl text-red-400">
        {error.message}
      </h2>
    );
  }

  return (
    <Layout>
      <div className="grid grid-col-1 md:grid-cols-2 lg:grid-cols-3">
        {data?.data?.results.map((item, index) => (
          <div key={index}>
            <CardArticleBusiness data={item} />
          </div>
        ))}
      </div>
      {/* <div className="flex justify-center p-4">
        {dataLength < loadData ? (
          <p className="text-lg text-gray-900 font-bold">Jumlah Data Maximal</p>
        ) : (
          <button
            onClick={() => setLoadData(loadData + 6)}
            className="px-12 py-1 bg-green-600 text-white rounded-sm"
            disabled={dataLength < loadData}
          >
            Load More
          </button>
        )}
      </div> */}
    </Layout>
  );
}

export default index;
