import React, { useState } from "react";
import { useQuery } from "react-query";
import axios from "axios";
import { useRouter } from "next/router";

function detail() {
  const router = useRouter();
  console.log(router.query.detail);
  const getMost = async () => {
    return axios.get(
      "https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?api-key=GYTWZG1F4BV2ZQ4GVlKPGt7nlcp6YCW0"
    );
  };
  const { isLoading, isError, data, error } = useQuery("most", getMost);
  if (isLoading) {
    return (
      <h2 className="flex justify-center mt-12 font-bold text-3xl text-gray-900">
        Loading...
      </h2>
    );
  }
  if (isError) {
    return (
      <h2 className="flex justify-center mt-12  text-3xl text-red-400">
        {error.message}
      </h2>
    );
  }
  const dataFilter = data?.data.results.filter(
    (data) => data.id == `${router.query.detail}`
  );
  console.log("dataFilter", dataFilter);
  return (
    <div className="flex justify-center items-center p-12">
      <button
        onClick={() => router.push("/artikel")}
        className="px-12 py-3 bg-green-500 text-white"
      >
        back
      </button>
      <div className=" border border-green-500 p-2">
        <p className="text-lg text-gray-600">{dataFilter[0]?.abstract}</p>
      </div>
    </div>
  );
}

export default detail;
