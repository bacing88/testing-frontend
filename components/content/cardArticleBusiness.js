import React, { useState } from "react";
import Image from "next/image";
import { EyeIcon } from "@heroicons/react/outline";
import Link from "next/link";
function cardArticle(item) {
  const gambar = item?.data?.media;
  const image = [];
  for (const property in gambar[0]) {
    image.push(gambar[0][property]);
  }
  const dataImage = image.slice(-1)[0];
  const dataX = image[5][2] ?? "testing";
  console.log("dataCard", data);
  return (
    <article className="p-1 mt-2 text-warna-400">
      <div className="block overflow-hidden rounded-lg">
        <div href="#" className="group relative block bg-black">
          <Image
            src={dataImage.url}
            alt="url"
            width={500}
            height={540}
            className="absolute inset-0 object-cover opacity-75 transition-opacity group-hover:opacity-50"
          />
          <div className="absolute p-4 -mt-48">
            <div className="translate-y-8 transform opacity-0 transition-all group-hover:translate-y-0 group-hover:opacity-100">
              <div className="flex justify-between">
                <p className="text-sm font-medium uppercase tracking-widest text-pink-500">
                  {item?.data.source}
                </p>
                <Link href={`/artikel/${item?.data.id}`}>
                  <EyeIcon className="h-5 w-5 text-white hover:text-pink-500" />
                </Link>
              </div>

              <p className="text-2xl font-bold">{item?.data.source}</p>
              <p className="text-sm line-clamp-3">
                {item?.data.source === ""
                  ? "No Content ..."
                  : item?.data.source}
              </p>
              <div className="p-1.5 mt-2 text-xs bg-warna-300 capitalize rounded-sm">
                author @{item?.data?.author ?? "No_Author"}
              </div>
            </div>
          </div>
        </div>

        <div className="bg-gray-900 p-4 -mt-1">
          <h5 className="text-md line-clamp-1">{item?.data.title}</h5>

          <p className="mt-0.5 px-1 text-xs text-gray-500 line-clamp-2">
            {item?.data.section}
          </p>
          <a href={item?.data?.url} target="_blank">
            <p className="text-xs text-gray-500 mt-3 hover ">Read More ...</p>
          </a>
        </div>
      </div>
    </article>
  );
}

export default cardArticle;
