import React from "react";
import Link from "next/link";
import { ClipboardCheckIcon } from "@heroicons/react/solid";

function cardDetailSwapi(data) {
  console.log("dataDetail", data);
  return (
    <div className="p-4 w-full">
      <ul className="my-4 space-y-3">
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Name</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.name}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Terrain</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.terrain}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Surface Water</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.surface_water}
            </span>
          </div>
        </li>

        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">
              Rotation Period
            </span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.rotation_period}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Population</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.population}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Diameter</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.diameter}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">
              Orbital Period
            </span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.orbital_period}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Gravity</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-md font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.gravity}
            </span>
          </div>
        </li>
      </ul>
    </div>
  );
}

export default cardDetailSwapi;
