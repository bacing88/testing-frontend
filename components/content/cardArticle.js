import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { EyeIcon, StarIcon } from "@heroicons/react/solid";

function cardArticle(item) {
  console.log(item);
  return (
    <article className="p-1 mt-2 text-warna-400">
      <div className="block overflow-hidden rounded-lg">
        <div href="#" className="group relative block bg-black">
          <Image
            src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2/${item?.data?.poster_path}`}
            alt="url"
            width={500}
            height={540}
            className="absolute inset-0 object-fill opacity-75 transition-opacity group-hover:opacity-50"
          />
          <div className="absolute p-4 -mt-48">
            <div className="translate-y-8 transform opacity-0 transition-all group-hover:translate-y-0 group-hover:opacity-100">
              <div className="flex justify-between">
                <p className="text-sm font-bold uppercase tracking-widest text-warna-400">
                  {item?.data.original_title}
                </p>
                <Link href={`/artikel/${item?.data.id}`}>
                  <EyeIcon className="h-5 w-5 text-white hover:text-pink-500" />
                </Link>
              </div>

              <p className="text-2xl font-bold">Movie</p>
              <p className="text-sm line-clamp-3">{item?.data.overview}</p>
              <div className="p-1.5 mt-2 text-xs bg-warna-300 capitalize rounded-sm">
                {item?.data?.release_date}
              </div>
            </div>
          </div>
        </div>

        <div className="bg-gray-900 p-4 -mt-1">
          <h5 className="text-md font-bold line-clamp-1">{item?.data.title}</h5>
          <div className="flex justify-between">
            <p className="text-sm text-warna-400">
              popularity : {item?.data?.popularity}
            </p>
            <p className="text-sm text-warna-400">
              {item?.data?.adult === false ? "Not Adult" : "Adult"}
            </p>
          </div>
          <div className="flex mt-1 items-center justify-between ">
            <div className="flex space-x-1">
              <StarIcon className="h-4 w-4 text-yellow-500" />
              <StarIcon className="h-4 w-4 text-yellow-500" />
              <StarIcon className="h-4 w-4 text-yellow-500" />
              <StarIcon className="h-4 w-4 text-yellow-500" />
              <StarIcon className="h-4 w-4 text-gray-400" />
            </div>

            <p className="px-1 text-xs text-yellow-500 line-clamp-2">
              {item?.data.vote_average}
            </p>
          </div>
        </div>
      </div>
    </article>
  );
}

export default cardArticle;
