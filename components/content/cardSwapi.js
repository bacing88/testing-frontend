import React from "react";
import Link from "next/link";
import { ClipboardCheckIcon } from "@heroicons/react/solid";

function cardSwapi(data) {
  const dataDetail = data?.data.url;
  const dataId = dataDetail.split("/");
  return (
    <div className="p-4 w-full bg-red-50">
      <div className="flex justify-between space-x-2">
        <h5 className="mb-3 text-base font-semibold text-gray-800 md:text-xl">
          {data?.data.name}
        </h5>
        <h5 className="mb-3 text-sm font-normal text-gray-800">
          <span>{data?.data.terrain}</span>
        </h5>
      </div>
      <ul className="my-4 space-y-3">
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Population</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-xs font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.population}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Diameter</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-xs font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.diameter}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">
              Orbital Period
            </span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-xs font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.orbital_period}
            </span>
          </div>
        </li>
        <li>
          <div className="flex items-center p-2 text-base font-bold text-gray-800 bg-gray-400 rounded-sm">
            <ClipboardCheckIcon className="w-5 h-5 text-gray-800" />
            <span className="flex-1 ml-3 whitespace-nowrap">Gravity</span>
            <span className="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-xs font-medium text-gray-400 bg-gray-600 rounded">
              {data?.data.gravity}
            </span>
          </div>
        </li>
      </ul>
      <div>
        <Link href={`/swapi/detail/${dataId[5]}`}>
          <a className="inline-flex items-center text-xs font-normal text-gray-800 hover:underline">
            show detail Planet ...
          </a>
        </Link>
      </div>
    </div>
  );
}

export default cardSwapi;
