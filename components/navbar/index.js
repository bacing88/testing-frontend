import React from "react";
//redux
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "../../slice/counterSlice";

function index() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();
  return (
    <nav className="flex items-center justify-between flex-wrap bg-warna-200 p-3 lg:p-4">
      <div className="flex items-center flex-shrink-0 text-warna-400 mr-6">
        <svg
          className="fill-current h-8 w-8 mr-2 hidden md:block"
          width="54"
          height="54"
          viewBox="0 0 54 54"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z" />
        </svg>
        <span className="font-semibold text-xl tracking-tight">
          newsapi.org
        </span>
      </div>
      <div
        className="inline-flex rounded-sm shadow-sm select-none -m-2 "
        role="group"
      >
        <button
          type="button"
          className="py-1 px-3 text-lg font-bold text-gray-900 bg-white rounded-l-sm border outline-none"
          onClick={() => dispatch(decrement())}
        >
          -
        </button>
        <button
          type="button"
          disabled={true}
          className="py-1 px-2 text-sm font-medium text-gray-900 bg-white border-t border-b outline-none"
        >
          Redux : {count}
        </button>
        <button
          type="button"
          className="py-1 px-3 text-lg font-bold text-gray-900 bg-white rounded-r-sm border outline-none"
          onClick={() => dispatch(increment())}
        >
          +
        </button>
      </div>
    </nav>
  );
}

export default index;
