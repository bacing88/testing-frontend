import React from "react";
import Navbar from "../components/navbar";

function Layout({ children }) {
  return (
    <div className="flex flex-col h-screen w-full ">
      <header>
        <Navbar />
      </header>
      <div className="overflow-auto scrollbar">
        <main>{children}</main>
      </div>
    </div>
  );
}

export default Layout;
