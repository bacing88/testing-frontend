import React from "react";
import Navbar from "../components/navbar/swapiNavBar";

function Layout({ children }) {
  return (
    <div className="flex flex-col h-screen w-full ">
      <header>
        <Navbar />
      </header>
      <div className="overflow-auto scrollbar bg-gray-300">
        <main>{children}</main>
      </div>
    </div>
  );
}

export default Layout;
