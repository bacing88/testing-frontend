/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        warna: {
          100: "#06283D",
          200: "#256D85",
          300: "#47B5FF",
          400: "#DFF6FF",
        },
        mode: {
          dark: "#2E2E33",
          subdark: "#52525A",
          DEFAULT: "#1D1D20",
          light: "#F4F4F8",
          sublight: "#A2A2A8",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
